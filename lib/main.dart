import 'package:flutter/material.dart';
import 'package:monitorc_mobile/app.dart';

<<<<<<< HEAD
void main() => runApp(const MyApp());
=======
import 'package:provider/provider.dart';

import 'src/view/home/home_page.dart';
import 'src/view/signin/signin_page.dart';
import 'src/providers/user_provider.dart';
import 'src/services/socket_client_service.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => UserProvider()),
        ChangeNotifierProvider(create: (_) => SocketClient())
      ],
      child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Monitor °C',
          initialRoute: 'signin',
          routes: {
            'signin': (context) => const Signin(),
            'home': (context) => const Home(),
          }),
    );
  }
}
>>>>>>> e783315dacc6c28ae4ae6d166b4ea268a25d6881
