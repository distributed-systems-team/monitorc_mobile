import 'package:flutter/material.dart';
import 'package:monitorc_mobile/src/pages/home/home_page.dart';
import 'package:monitorc_mobile/src/pages/sign_in/sign_in_page.dart';
import 'package:monitorc_mobile/src/providers/user_provider.dart';
import 'package:monitorc_mobile/src/services/socket_client_service.dart';
import 'package:provider/provider.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  /// This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(create: (_) => UserProvider()),
          ChangeNotifierProvider(create: (_) => SocketClient())
        ],
        child: MaterialApp(
            debugShowCheckedModeBanner: false,
            title: 'Monitor °C Software',
            initialRoute: SignInPage.ROUTE,
            routes: {
              SignInPage.ROUTE: (context) => const SignInPage(),
              Home.ROUTE: (context) => const Home()
            }));
  }
}
