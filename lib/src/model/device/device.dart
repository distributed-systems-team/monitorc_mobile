class Device {
  String id;
  String name;
  double lastTemperature;
  double lastHumidity;
  String dataLastUpdated;

  Device(
      {this.id = "",
      this.name = "",
      this.lastTemperature = -271.1,
      this.lastHumidity = - -271.1,
      this.dataLastUpdated = '1970-01-01 00:00:00'});

  List<Device> getDevices() {
    return List.generate(
        100,
        (index) => Device(
            id: "$index",
            name: "Device Name $index",
            lastTemperature: 15,
            lastHumidity: 15,
            dataLastUpdated: "2022-05-30 18:50:00"));
  }
}
