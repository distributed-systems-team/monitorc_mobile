import 'package:flutter/material.dart';

class UserProvider extends ChangeNotifier {
  String _email;

  UserProvider() : _email = "";

  String get email => _email;

  set email(String email) {
    _email = email;
    notifyListeners();
  }
}
