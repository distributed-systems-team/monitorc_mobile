import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'widgets/sign_in_form.dart';

class SignInPage extends StatelessWidget {
  // ignore: non_constant_identifier_names
  static String ROUTE = "sign_in_page";
  const SignInPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        body: Stack(fit: StackFit.expand, children: [
          SvgPicture.asset("assets/images/background.svg", fit: BoxFit.fill),
          SingleChildScrollView(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                SvgPicture.asset("assets/images/tablet-login-rafiki.svg",
                    semanticsLabel: 'login_logo', height: 300),
                const Text("Inicio de Sesión",
                    style: TextStyle(
                        fontStyle: FontStyle.italic,
                        fontSize: 15,
                        color: Color.fromRGBO(89, 88, 90, 1.0))),
                Container(
                  padding: const EdgeInsets.fromLTRB(20, 30, 20, 10),
                  child: const SignInForm(),
                ),
                _signUpButton(context),
                _forgotPasswordButton(context)
              ]))
        ]));
  }

  Widget _signUpButton(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const Text(
          "¿No tienes una Cuenta?",
          style: TextStyle(color: Color.fromRGBO(89, 88, 90, 1.0)),
        ),
        TextButton(
          child: const Text(
            "Regístrate",
            style: TextStyle(color: Colors.indigo),
          ),
          onPressed: () => Navigator.of(context).pushNamedAndRemoveUntil(
              'sign_up_page', (Route<dynamic> route) => true),
        ),
      ],
    );
  }

  Widget _forgotPasswordButton(BuildContext context) {
    return TextButton(
      child: const Text(
        "Olvidé mi contraseña",
        style: TextStyle(color: Colors.indigo),
      ),
      onPressed: () => Navigator.of(context).pushNamedAndRemoveUntil(
          'forgot_password', (Route<dynamic> route) => true),
    );
  }
}
