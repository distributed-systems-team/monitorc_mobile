// ignore_for_file: unused_field

import 'package:flutter/material.dart';
import 'package:monitorc_mobile/src/pages/home/home_page.dart';

import '../../../services/signin_validator_service.dart';
import 'button.dart';
import 'textformfield.dart';

class SignInForm extends StatefulWidget {
  const SignInForm({Key? key}) : super(key: key);

  @override
  State<SignInForm> createState() => _SignInFormState();
}

class _SignInFormState extends State<SignInForm> {
  final _formKey = GlobalKey<FormState>();
  // ignore: unused_field
  String _password = "";
  String _email = "";
  bool _obscurePasswordState = true;
  // SocketClient _socketClient = SocketClient();

  @override
  Widget build(BuildContext context) {
    // _socketClient = Provider.of<SocketClient>(context);
    return Form(
        key: _formKey,
        child: Column(children: <Widget>[
          _userTextFormField(),
          const SizedBox(height: 15),
          _passwordTextFormField(),
          const SizedBox(height: 20),
          ButtonP(label: "Iniciar Sesión", onPressed: _signIn)
        ]));
  }

  TextFormFieldP _userTextFormField() {
    return TextFormFieldP(
      onChanged: (String value) => _email = value,
      validator: (value) {
        if (value!.isEmpty) return 'Escribe tu email!';
        return !SignInValidator.validateEmail(value)
            ? 'Escribe un Correo Valido!'
            : null;
      },
      keyboardType: TextInputType.emailAddress,
      labelText: "correo",
      hintText: "mycorreo@gmail.com",
    );
  }

  TextFormFieldP _passwordTextFormField() {
    Icon suffixIcon = const Icon(Icons.lock_open, color: Colors.indigo);
    if (_obscurePasswordState) {
      suffixIcon = const Icon(Icons.lock_outlined, color: Colors.indigo);
    }
    return TextFormFieldP(
      keyboardType: TextInputType.visiblePassword,
      validator: (value) {
        if (value!.isEmpty) return 'Escribe tu contraseña!';
        return !SignInValidator.validatePassword(value)
            ? 'Contraseña Incorrecta'
            : null;
      },
      onChanged: (value) {
        _password = value;
      },
      obscureText: _obscurePasswordState,
      labelText: "contraseña",
      hintText: "mayonesa345.?",
      suffixIcon: TextButton(
          onPressed: () => setState(() {
                _obscurePasswordState = !_obscurePasswordState;
              }),
          child: suffixIcon),
    );
  }

  Future<void> _signInRequest(BuildContext context) async {
    // HttpClient httpClient = HttpClient();
    // Map response = await httpClient.post('/user_management/user_manage/sign_in',
    //     {'email': _email, 'password': _password});
    // if (response['status'] == 200) {
    //   await _socketClient.connect();
    //   Navigator.of(context)
    //       .pushNamedAndRemoveUntil('home', (Route<dynamic> route) => true);
    // }
    Navigator.of(context)
        .pushNamedAndRemoveUntil(Home.ROUTE, (Route<dynamic> route) => true);
  }

  void _signIn() {
    if (_formKey.currentState!.validate()) _signInRequest(context);
  }
}
