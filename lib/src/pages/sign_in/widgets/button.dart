import 'package:flutter/material.dart';

class ButtonP extends StatelessWidget {
  final void Function()? onPressed;
  final String? label;
  final Color? primary;
  final Size? fixedSize;
  const ButtonP(
      {Key? key,
      @required this.onPressed,
      @required this.label,
      this.primary = Colors.indigo,
      this.fixedSize = const Size(320, 40)})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onPressed,
      style: ElevatedButton.styleFrom(
          primary: primary,
          onPrimary: Colors.white,
          fixedSize: fixedSize,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20.0))),
      child: Text(
        label!,
        style: const TextStyle(
          fontSize: 17,
          fontStyle: FontStyle.italic,
        ),
      ),
    );
  }
}
