import 'package:dio/dio.dart';

class HttpClient {
  final String _origin;
  final Dio _dio;

  HttpClient()
      : _origin = "http://192.168.100.47:3000",
        _dio = Dio();

  ///Get Method for a Http Resquest
  Future<Map> get(String route) async {
    try {
      Response response = await _dio.get(_origin + route);
      return response.data;
    } catch (e) {
      return {"error": "ERROR_WITH_GET_METHOD"};
    }
  }

  ///Post Method for a Http Resquest
  Future<Map> post(String route, dynamic data) async {
    try {
      Response response = await _dio.post(_origin + route, data: data);
      return response.data;
    } catch (e) {
      return {"error": "ERROR_WITH_POST_METHOD"};
    }
  }
}
