import 'package:flutter/material.dart';
import "package:socket_io_client/socket_io_client.dart" as io;

enum ServerStatus { online, offline, connecting }

class SocketClient with ChangeNotifier {
  io.Socket? _socket;
  ServerStatus _serverStatus = ServerStatus.offline;

  ServerStatus get serverStatus => _serverStatus;
  io.Socket? get socket => _socket;

  Future<void> connect() async {
    _socket = io.io(
        "http://192.168.100.47:3000",
        io.OptionBuilder()
            .setTransports(["websocket"])
            .disableAutoConnect()
            .setExtraHeaders({'x-access-token': 'token-here'})
            .build());
    _serverStatus = ServerStatus.offline;
    _socket!.connect();
    _socket!.onConnect((_) {
      _serverStatus = ServerStatus.online;
      notifyListeners();
    });
    _socket!.onDisconnect((_) {
      _serverStatus = ServerStatus.offline;
      notifyListeners();
    });
  }

  void disconnect() {
    _socket!.disconnect();
    _socket!.onDisconnect((_) {
      _serverStatus = ServerStatus.offline;
      notifyListeners();
    });
  }
}
