class SignInValidator {
  static final RegExp regExp = RegExp(
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');

  /// email validator process
  static bool validateEmail(String email) {
    return regExp.hasMatch(email);
  }

  /// password validator process
  static bool validatePassword(String password) {
    return password.length > 5;
  }
}
