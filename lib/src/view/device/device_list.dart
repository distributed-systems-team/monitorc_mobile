import 'package:flutter/material.dart';

import '../../model/device/device.dart';

class DeviceList extends StatefulWidget {
  const DeviceList({Key? key}) : super(key: key);

  @override
  State<DeviceList> createState() => _DeviceListState();
}

class _DeviceListState extends State<DeviceList> {
  @override
  Widget build(BuildContext context) {
    return ListView(
        padding: const EdgeInsets.all(20), children: _getDeviceCard());
  }

  List<Widget> _getDeviceCard() {
    List<Widget> devicesCards = [];
    Device device = Device();
    List<Device> deviceList = device.getDevices();
    Card deviceCard;
    for (Device device in deviceList) {
      deviceCard = Card(
        elevation: 5,
        shadowColor: Colors.black,
        borderOnForeground: false,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        child: ListTile(
            contentPadding: const EdgeInsets.fromLTRB(15, 5, 15, 15),
            title: Text(device.name),
            leading: Text(device.id),
            minLeadingWidth: 15,
            subtitle:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text("Temperatura ${device.lastTemperature} °C"),
              Text("Humedad ${device.lastHumidity} %"),
              Text("Ult. Vez ${device.dataLastUpdated}")
            ]),
            trailing: _getIconStatus(device.dataLastUpdated)),
      );
      devicesCards.add(deviceCard);
    }
    return devicesCards;
  }

  Icon _getIconStatus(String dataLastUpdated) {
    Icon icon = Icon(
      Icons.signal_wifi_4_bar,
      color: Colors.lightGreenAccent.shade200,
    );
    DateTime lastDate = DateTime.parse(dataLastUpdated);
    Duration differences = DateTime.now().difference(lastDate);
    if (differences.inMinutes > 5) {
      icon = const Icon(Icons.signal_wifi_off);
    }
    return icon;
  }
}
