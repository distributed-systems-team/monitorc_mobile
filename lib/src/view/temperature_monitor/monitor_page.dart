import 'package:flutter/material.dart';

class Monitor extends StatefulWidget {
  const Monitor({Key? key}) : super(key: key);

  @override
  State<Monitor> createState() => _MonitorState();
}

class _MonitorState extends State<Monitor> {
  String _temperature = "45 °C";

  @override
  Widget build(BuildContext context) {
    return Text(_temperature);
  }
}
