import 'package:flutter/material.dart';

import '../device/device_list.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);
  // ignore: non_constant_identifier_names
  static String ROUTE = "home_page";
  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  int _selectedIndex = 0;
  final List<Widget> _listPages = <Widget>[
    const DeviceList(),
    const Text("Page 2"),
    const Text("Page 3"),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue[50],
      body: Center(child: _listPages.elementAt(_selectedIndex)),
      bottomNavigationBar: BottomNavigationBar(
        items: const [
          BottomNavigationBarItem(icon: Icon(Icons.monitor_rounded), label: ""),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.phone,
              ),
              label: ""),
          BottomNavigationBarItem(icon: Icon(Icons.phone), label: ""),
        ],
        currentIndex: _selectedIndex,
        onTap: (index) {
          setState(() {
            _selectedIndex = index;
          });
        },
      ),
    );
  }
}
