import 'package:flutter/material.dart';

class TextFormFieldP extends StatelessWidget {
  final String? Function(String?)? validator;
  final void Function(String)? onChanged;
  final TextInputType? keyboardType;
  final bool obscureText;
  final String? hintText;
  final String? labelText;
  final Widget? icon;
  final Widget? suffixIcon;

  const TextFormFieldP(
      {Key? key,
      this.validator,
      this.onChanged,
      this.keyboardType,
      this.obscureText = false,
      this.hintText,
      this.labelText,
      this.icon,
      this.suffixIcon})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      keyboardType: keyboardType,
      cursorColor: Colors.indigo,
      style: const TextStyle(
        color: Color.fromRGBO(89, 88, 90, 1.0),
        fontStyle: FontStyle.italic,
      ),
      decoration: _inputDecoration(),
      validator: validator,
      onChanged: onChanged,
      obscureText: obscureText,
    );
  }

  InputDecoration _inputDecoration() {
    return InputDecoration(
        focusColor: Colors.indigo,
        fillColor: const Color.fromRGBO(237, 248, 255, 1.0),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        filled: true,
        hintText: hintText,
        labelText: labelText,
        icon: icon,
        suffixIcon: suffixIcon);
  }
}
