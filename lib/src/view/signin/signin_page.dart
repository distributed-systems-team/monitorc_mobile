import 'package:flutter/material.dart';

import 'widgets/signin_form.dart';

class Signin extends StatelessWidget {
  const Signin({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
            child: Column(
          children: <Widget>[
            const Text(
              "Inicio de Sesión",
            ),
            Container(
              padding: const EdgeInsets.fromLTRB(20, 30, 20, 10),
              child: const SigninForm(),
            ),
            _signupButton(context),
            _forgotPasswordButton(context)
          ],
        )),
      ),
    );
  }

  Widget _signupButton(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const Text(
          "¿No tienes una Cuenta?",
          style: TextStyle(color: Color.fromRGBO(89, 88, 90, 1.0)),
        ),
        TextButton(
          child: const Text(
            "Registrate",
            style: TextStyle(color: Colors.blue),
          ),
          onPressed: () => Navigator.of(context).pushNamedAndRemoveUntil(
              'signup', (Route<dynamic> route) => true),
        ),
      ],
    );
  }

  Widget _forgotPasswordButton(BuildContext context) {
    return TextButton(
      child: const Text(
        "Olvide my contraseña",
        style: TextStyle(color: Colors.blue),
      ),
      onPressed: () => Navigator.of(context).pushNamedAndRemoveUntil(
          'forgot_password', (Route<dynamic> route) => true),
    );
  }
}
